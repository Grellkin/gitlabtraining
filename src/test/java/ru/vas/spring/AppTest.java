package ru.vas.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void name() {
        App app = Mockito.mock(App.class);
        Mockito.when(app.sayHello()).thenReturn("Hello!");
        assertEquals(app.sayHello(), "Hello!");

    }

    @Test
    public void some(){
        assertEquals(1, 11);
    }
}
